function expendMe(elm) {
    let parent = elm.parentNode.parentNode;
    if(!parent.classList.contains('active')){
        elm.innerHTML = '&#9650;';
        parent.classList.add('active');
    }else{
        elm.innerHTML = '&#9660;';
        parent.classList.remove('active');
    }
}