<html>
<head>
	<style>
	body{font-family:calibri;position:relative;height:100%;overflow: hidden;}
	.error {color:#FF0000;}
	.chat-connection-ack{color: #26af26;}
	.chat-message {border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;
	}
	#btnSend {
		padding:0;
		margin:0;
		height:0;
		border:none;
	}
	#chat-box {border: 1px solid gray;border-radius: 4px;border-bottom-left-radius:0px;border-bottom-right-radius: 0px;
		height: calc(100% - 115px);
		padding: 10px;overflow: auto;
	}
	.chat-box-html{color: #09F;margin: 10px 0px;font-size:0.8em;}
	.chat-box-message{color: #09F;padding: 5px 10px; background-color: #fff;border: 1px solid gray;border-radius:4px;display:inline-block;}
	.chat-input{border: 1px solid gray;border-top: 0px;width: 100%;box-sizing: border-box;padding: 10px 8px;color: #191919;
	}
	input:focus{
		outline:none;
	}
	.me{
		text-align: right;
	}
	.me > li{
		background: #cbf8cd;
		margin-left:40%;
	}
	.dude > li{
		background: whitesmoke;
	}
	.bubble > li{
		border-radius: 5px;
		padding: 5px;
		max-width: 60%;
		display:inline-block;
	}
	.bubble {
		margin-top: 5px;
	}
	.bubble > small{
		margin-top: 2px;
		display: block;
		font-size: 11px;
		color: lightgray;
	}
	#frmChat{
		width: 50%;
		margin:auto;
		height:100%;
	}
	#chat-box::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 0 rgba(0,0,0,0.3);
		border-radius: 10px;
		background-color: #F5F5F5;
	}

	#chat-box::-webkit-scrollbar
	{
		width: 0;
		background-color: #F5F5F5;
	}

	#chat-box::-webkit-scrollbar-thumb
	{
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 0 rgba(0,0,0,.3);
		background-color: #D62929;
	}
	</style>	
	<script src="jQuery.js"></script>
	<script>  
	function showMessage(messageHTML) {
		$('#chat-box').append(messageHTML);
	}
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December']
	function getTime(){
		const date = new Date();
		var n = date.toLocaleString([], { hour12: true});
		return date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear() + ", " + n.split(', ')[1];
	}
	$div = document.createElement('DIV');
	$span = document.createElement('LI');
	$timestamp = document.createElement('SMALL');
	function appendMessage(message, me = true){
		$bubble = $div.cloneNode(true);
		$span = $span.cloneNode(true);
		$time = $timestamp.cloneNode();
		$nl = $div.cloneNode(true);
		$bubble.classList.add('bubble');
		$bubble.classList.add(me ? 'me' : 'dude');
		$time.innerHTML = getTime();
		$span.innerHTML = message;
		$($bubble).append($span);
		$($bubble).append($nl);
		$($bubble).append($time);
		$('#chat-box').append($bubble);
	}
	$(document).ready(function(){
		var _chat = function(callback){
			if(!callback.name || callback.name == ''){
				throw new Error("name is requires.");
			}
			if(!callback.onmessage || callback.onmessage == ''){
				throw new Error("onmessage function is requires.");
			};
			if(!callback.onopen || callback.onopen == ''){
				callback['onopen'] = function(event) {
					console.log('%cconnection opned', `color: green`)
				}
			};
			if(!callback.onerror || callback.onerror == ''){
				callback['onerror'] = function(event) {
					console.log('%cconnection error.', `color: red`)
				}
			};
			if(!callback.onclose || callback.onclose == ''){
				callback['onclose'] = function(event) {
					console.log('%cconnection closed.', `color: red`)
				}
			};
			var websocket = new WebSocket("ws://127.0.0.1:8090/chat?" + callback.name); 
			websocket.onopen = function(event){
				callback.onopen(event)
			};
			websocket.onmessage = function(event) {
				callback.onmessage(JSON.parse(event.data))
			};
			websocket.onerror = function(event){
				callback.onerror(event)
			};
			websocket.onclose = function(event){
				callback.onclose(event)
			};
			return websocket;
		} 
		var webskt = _chat({
				name:"<?=$_GET['name']?>", 
				onmessage:function(data){
					if(data.message){
						if(data.message != 'connection ACK.'){
							appendMessage(data.message, false);
						}
					}
				},
			})
		$('#frmChat').on("submit",function(event){
			event.preventDefault();	
			var messageJSON = {
				from: $('#from').val(),
				to: $('#to').val(),
				message: $('#chat-message').val()
			};
			appendMessage($('#chat-message').val());
			$('#chat-message').val('');
			webskt.send(JSON.stringify(messageJSON));
		});
	});




	</script>
	</head>
	<body>
		<form name="frmChat" id="frmChat">
			<div id="chat-box">
			</div>
			<input type="hidden" name="from" id="from" placeholder="Name" class="chat-input" value="<?=$_GET['name']?>" />
			<input type="text" name="to" id="to" placeholder="Name" class="chat-input" />
			<input type="text" name="chat-message" id="chat-message" placeholder="Message"  class="chat-input chat-message" />
			<input type="submit" id="btnSend" name="send-chat-message" value="" >
		</form>
</body>
</html>