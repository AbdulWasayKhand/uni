<style>
*{font-size:15px;}
table{border-collapse: collapse;}
div{border:1px solid gray;padding:10px;margin:10px;}
td{padding:5px;}
li{padding:10px;word-break: break-all;}
span{color:darkslategray;text-transform: uppercase;}
li:nth-child(odd){color:#ffb122;}
li:nth-child(even){color:#83de83;}
.abc{margin:0;padding:0; list-style:none;}
ul.abc>li:nth-child(odd){color:#678dd2;}
ul.abc>li:nth-child(even){color:#ffb122;}
h1{text-align:center;text-transform: uppercase;}
</style>
<div>
    <h1>Server side with PHP</h1>
    
    <?php require_once('e.php');
        $t = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
        $t = 'shafi.catcos@gmail.com';
        $p = '@ qu!ck br0wn f0x jumps 0v3r the l@zy d0g ';
        $p = 'H!rch33kM!rCh!kDh@n@Dh33rch!kAn9P@t33n9@nN@9a9nJ09@nKh@r@Khut33But33Cht33K!ru';
        $p2 = 'A Quick bron Fox Jumps Over The Lazy DogA Quick bron Fox Jumps Over The Lazy DogA Quick bron Fox Jumps Over The Lazy Dog';
        echo '<ul><li><span>Plain Text: </span>' . $t . '</li><li><span>Key: </span>' . $p . '</li></ul>';
        $enc = e($t, $p);
        echo '<ul class="abc"><li><span>Cipher Product: </span>' . $enc . '</li><li><span>Recoverd Text: </span>' . e($enc, $p) . '</li></ul>';
    ?>
</div>

<div id="msg">
    <h1>Client side with Javascript</h1>
</div>
<script src="e.php?e"></script>
<script>
var t = '<?= $t ?>';
var p = '<?= $p ?>';
var p2 = '<?= $p2 ?>';
var txt = e(t, p, 1);
// var txt = '<?= e($t, $p, true) ?>';
msg.innerHTML += '<ul><li><span>Plain Text: </span>' + t + '</li><li><span>Key: </span>' + p + '</li></ul>';
msg.innerHTML += '<ul class="abc"><li><span>Cipher Product: </span>' + txt + '</li><li><span>Recoverd Text: </span>' +  e(txt, p) + '</li></ul>';
// var data = {
//     'php' : {
//         'text'  : <?= strlen($t) ?>,
//         'key'   : <?= strlen($p) ?>
//     },
//     'javascript': {
//         'text'  : t.length,
//         'key'   : p.length
//     }
// };
// var ttt2 = e(15, '^|p`ttt|^');
// console.log(ttt2);


// msg.innerHTML += power(128, 343)%BigInt(527);
// function power(b, p){
//     var v = BigInt(b);
//     for(i = 2; i <= p; i++){
//         v = BigInt(v) * BigInt(b);
//     }
//     return v;
// }
// 17, 18 bin
// 15, 16 hex
</script>