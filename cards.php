<link rel="stylesheet" href="gridview.css">
<style>
    @media only screen and (max-width: 1200px) {
        .card-header {
            width:calc(100% - 10px);
            background-color: gray;
            display: inline-block;
            padding: 5px;
            color:whitesmoke;
        }
        .card-header > div {
            float: left;
        }
        .md-card {
            padding: 0!important;
        }
        .card-body {
            margin: 10px 0 10px 0;
            padding: 5px;
        }
        .card-header > div:nth-child(2) {
            text-align: right;
        }
        .card-header:not(.active) ~ * {
            display: none;
        }
        .card-header:hover  ~ * {
            display: block;
        }
        .exp-btn {
            cursor: pointer;
        }
        .md-card:hover .card-title, .active .card-title {
            color: transparent;
        }
    }
</style>
<script src="gridview.js" defer></script>
<?php
    $data = [
        'Heading One',
        'Heading Two',
        'Heading Three',
        'Heading Four',
        'Heading Five'
    ];
    $rows = [$data,$data,$data,$data,$data];
?>
<div class="container">
    <h1>Expendable Table</h1>
    <div class="table lg-card-only">
        <?php foreach($rows as $key => $row){ ?>
            <div class="tr">
                <div class="md-card">
                    <div class='card-header <?= $key == 0 ? "active" : "" ?>'>
                        <div class="lg-11 md-show card-title">
                            Data Value <?= ($key + 1) ?>
                        </div>
                        <div class="lg-1">
                            <span onclick="expendMe(this)" class='exp-btn md-show'><?= $key != 0 ? "&#9660;" : "&#9650;" ?></span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col xs-12 sm-12 md-8 lg-4">
                            <div class="th">
                                <?= $row[0] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-4 lg-2">
                            <div class="th">
                                <?= $row[1] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-5 lg-2">
                            <div class="th">
                                <?= $row[2] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-4 lg-2">
                            <div class="th">
                                <?= $row[3] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-3 lg-2">
                            <div class="th">
                                <?= $row[4] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="container">
    <h1>Sticked Table</h1>
    <div class="table lg-card-only">
        <?php foreach($rows as $key => $row){ ?>
            <div class="tr">
                <div class="md-card">
                    <div class="card-body">
                        <div class="col xs-12 sm-12 md-8 lg-4">
                            <div class="th">
                                <?= $row[0] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-4 lg-2">
                            <div class="th">
                                <?= $row[1] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-5 lg-2">
                            <div class="th">
                                <?= $row[2] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-4 lg-2">
                            <div class="th">
                                <?= $row[3] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                        <div class="col xs-6 sm-6 md-3 lg-2">
                            <div class="th">
                                <?= $row[4] ?>
                            </div>
                            <div class="td">
                                Data Value <?= ($key + 1) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>