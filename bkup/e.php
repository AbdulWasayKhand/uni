<?php

function e2(...$a){
    switch($a[0]){
        default : 
        return !strpos($a[0], '|SQZ|') ? e2(119, e2(112, e2(112, e2(112, e2(110, e2(106, $a[0], $a[1]), $a[1]), $a[1], 0), $a[1], 2), $a[1], 1), $a[1]) : e2(106, e2(110, e2(112, e2(112, e2(112, e2(119, $a[0], $a[1]), $a[1], 1), $a[1], 2), $a[1], 0), $a[1]), $a[1]); // Transposition
        case 101 : // Master Table
            $mk = '[MlU% (Jdiv;5S7oz,qwZ0h4Vn6EPWHQ!CTj)@.r|Og:DY&spxc1m{2bXLktyfu$*K9aeGA]FNR8BI}-3_^~>`=/<+?';
        return isset($a[1]) ? substr($mk,0, strlen($mk) - 10) : $mk;
        case 102 : // Original Array
        return str_split(e2(101));
        case 103 : // First Substitution Table
            $permutiveArray = e2(102);
            $subtituteTable = [];
            foreach (e2(102) as $k => $v) {
                if ($k != 0) {
                    $permutiveValue = array_shift($permutiveArray);
                    $permutiveArray[] = $permutiveValue;
                }
                $subtituteTable[] = $permutiveArray;
            }
        return $subtituteTable;
        case 104 : // First Encipherment
        return !in_array($a[2], e2(102)) || !in_array($a[1], e2(102))  ? $a[1] : e2(103)[array_search($a[2], e2(102))][array_search($a[1], e2(102))];
        case 105 : // First Dicipherment
        return  !in_array($a[2], e2(102)) || !in_array($a[1], e2(102)) ? $a[1] : e2(102)[array_search($a[1], e2(103)[array_search($a[2], e2(102))])];
        case 106 : // First encipher and decipher converssion
            $flag = 0;
            $data = [
                'pt' => $a[1],
                'key' => $a[2]
            ];
            if (strpos($a[1], '^|p`t|^')) {
                $data['pt'] = explode('^|p`t|^', $a[1])[0];
            }
            $str_s = mb_strlen($data['pt']);
            $key_s = mb_strlen($data['key']);
            if ($str_s > $key_s) {
                $ni = $str_s / $key_s;
                $pre_permutive = $data['key'];
                $flag = 1;
            } else if ($str_s < $key_s) {
                $ni = $key_s / $str_s;
                $pre_permutive = $data['pt'] . '^|x`t|^';
                $flag = 2;
            }
            $substitute = '';
            if (isset($ni)) {
                for ($i = 0; $i < $ni; $i++) {
                    $substitute .= $pre_permutive;
                }
            }
            if ($flag == 1) {
                $data['key'] = substr($substitute, 0, $str_s);
            } else if ($flag == 2) {
                $data['pt'] = substr($substitute, 0, $key_s);;
            }
            $txt = '';
            if (!strpos($a[1], '^|p`t|^')) {
                foreach (str_split($data['pt']) as $k => $v) {
                    $txt .= e2(104,$data['pt'][$k] ?? '', $data['key'][$k] ?? '');
                }
                return $txt . '^|p`t|^';
            }
            foreach (str_split($data['pt']) as $k => $v) {
                $txt .= e2(105, $data['pt'][$k], $data['key'][$k]);
            }
        return explode('^|x`t|^',$txt)[0];
        case 107 : 
        return array_chunk($a[1], $a[2]);
        case 108 : // Second  Substitution Table
            $squizedKey  = [];
            $split = str_split($a[1] . e2(101,1));
            foreach($split as $v){
                if(array_search($v, $squizedKey) === false){
                    $squizedKey[] = $v;
                }
            }
        return e2(107, $squizedKey, 9);
        case 109 : // Get Operated Cipher for Secons Substitution
            $holder = [];
            $product = [];
            foreach($a[2] as $key=>$row){
                if(in_array($a[1][0],$row)){
                    $holder[0] = [$key, array_search($a[1][0], $row)];
                }
            }
            foreach($a[2] as $key=>$row){
                if(in_array($a[1][1],$row)){
                    $holder[1] = [$key, array_search($a[1][1], $row)];
                }
            }
            $row = $holder[0][0] == $holder[1][0];
            $col = $holder[0][1] == $holder[1][1];
            if($a[3]){
                if($row){
                    $product = [$holder[0][1] == 8 ? $a[2][$holder[0][0]][0] : $a[2][$holder[0][0]][$holder[0][1] + 1], $holder[1][1] == 8 ? $a[2][$holder[0][0]][0] : $a[2][$holder[0][0]][$holder[1][1] + 1]];
                }elseif($col){
                    $product = [$a[2][$holder[0][0] != 8 ? ($holder[0][0] + 1) : 0][$holder[0][1]], $a[2][$holder[1][0] != 8 ? ($holder[1][0] + 1) : 0][$holder[1][1]]];
                }else{
                    $product = [$a[2][$holder[1][0]][$holder[0][1]], $a[2][$holder[0][0]][$holder[1][1]]];
                }
            }else{
                if($row){
                    $product = [$holder[0][1] == 0 ? $a[2][$holder[0][0]][8] : $a[2][$holder[0][0]][$holder[0][1] - 1], $holder[1][1] == 0 ? $a[2][$holder[0][0]][8] : $a[2][$holder[0][0]][$holder[1][1] - 1]];
                }elseif($col){
                    $product = [$a[2][$holder[0][0] == 0 ? 8 : ($holder[0][0] - 1)][$holder[0][1]], $a[2][$holder[1][0] == 0 ? 8 : ($holder[1][0] - 1)][$holder[1][1]]];
                }else{
                    $product = [$a[2][$holder[1][0]][$holder[0][1]], $a[2][$holder[0][0]][$holder[1][1]]];
                }
            }
        return $product;
        case 110 : // Second encipher and decipher converssion
            $op = true;
            if(strpos($a[1], '^|p`tt|^')){
                $a[1] = explode('^|p`tt|^', $a[1])[0];
                $op = false;
            }
            $c = '';
            if(strlen($a[1])%2){
                $a[1] .= '|^f^|';
            }
            $a[1] = e2(107, str_split($a[1]), 2);
            foreach($a[1] as $ch){
                if(in_array($ch[0], str_split(e2(101,1))) === false || in_array($ch[1], str_split(e2(101,1))) === false){
                    $c .= implode('', $ch);
                }else{
                    $c .= implode('', e2(109,$ch, e2(108, $a[2]), $op));
                }
            }
        return str_replace('|^f^|','', $c) . ($op ? '^|p`tt|^' : '');
        case 112 : // Transposition
            $a[3] = $a[3] ?? 0;
            $op = strpos($a[1],($a[3] ? ($a[3] == 1 ? '010111100111110001110000011000000111010001110100011101000111110001011110' : '5e7c70607474747c5e') : '^|p`ttt|^'));
            $a[1] = explode(($a[3] ? ($a[3] == 1 ? '010111100111110001110000011000000111010001110100011101000111110001011110' : '5e7c70607474747c5e') : '^|p`ttt|^'), $a[1])[0];
            $ky = str_split(strlen($a[2]));
            $g = 0;
            while(count($ky) % 2){
                $ky[] = $g++;
            }
            $sort = $ky;
            sort($sort);
            if($op){
                $sort = e2(114, $ky, $sort);
                $m=e2(113,e2(107,str_split($a[1]), strlen($a[1])/count($ky)));
                $m=e2(113,$m);
                $n=[];
                foreach($sort as $v){
                    $n[] = $m[$v];
                }
                $m=e2(113,$n);
                $n='';
                foreach($m as $v){
                    $n .= implode('', $v);
                }
                return explode('^|f`g|^', ($a[3] ? ($a[3] == 1 ? e2(118, $n) : e2(116, $n)): $n ))[0];
            }
            $sort = e2(114, $sort, $ky);
            $f= ($a[3] ? ($a[3] == 1 ? e2(117, $a[1]) : e2(115,$a[1])) : $a[1]);
            if(strlen($f)%count($ky)){
                $f .= ($a[3] ? ($a[3] ? '01011110011111000110011001100000011001110111110001011110' : '5e7c6660677c5e') : '^|f`g|^');
            }
            while(intval(strlen($f)/count($ky)) !== strlen($f)/count($ky)){
                $f .= ($a[3] ? ($a[3] == 1 ? '01111100' : '7c') : '|');
            }
            $f=str_split($f);
            $g=e2(113,e2(107,$f,count($ky)));
            $h='';
            foreach($sort as $v){
                $h .= implode('', $g[$v]);
            }
        return $h.($a[3] ? ($a[3] == 1 ? '010111100111110001110000011000000111010001110100011101000111110001011110' : '5e7c70607474747c5e') : '^|p`ttt|^' );
        case 113 : // Transposing Rows and Columns
            $s = [];
            foreach($a[1][0] as $k => $v){
                $s[] = array_column($a[1], $k);
            }
        return $s;
        case 114 :
            $d = [];
            foreach($a[1] as $v){
                $s = array_search($v,$a[2]);
                $d[] = $s;
                $a[2][$s] = 'd';
            }
        return $d;
        case 115 : // Converting binary to hex
        return bin2hex($a[1]);
        case 116 : // Converting hex to string
        return hex2bin($a[1]);
        case 117 :
            $b = '';
            foreach(str_split($a[1]) as $v){
                $b .=  str_pad(base_convert(e2(115,$v), 16, 2), 8 , '0',  STR_PAD_LEFT);
            }
        return $b;
        case 118 :
            $b = '';
            foreach(e2(107,str_split($a[1]),8) as $v){
                $b .= pack('H*', dechex(bindec(implode('',$v))));
            }
        return $b;
        case 119:
            if(strpos($a[1], '|SQZ|')){
                $index = [
                    'A' => '0000',
                    'B' => '0001',
                    'C' => '0010',
                    'D' => '0011',
                    'E' => '0100',
                    'F' => '0101',
                    'G' => '0110',
                    'H' => '0111',
                    'I' => '1000',
                    'J' => '1001',
                    'K' => '1010',
                    'L' => '1011',
                    'M' => '1100',
                    'N' => '1101',
                    'O' => '1110',
                    'P' => '1111'
                ];
                $a[1] = e2(106, str_replace('|SQZ|','^|p`t|^',$a[1]), $a[2]);
                $b = str_split($a[1],1);
                $f='';
                $g=[];
                foreach($b as $k => $v){
                    if(is_numeric($v)){
                        $f .= $v;
                    }
                    if(!is_numeric($v)){
                        $g[] = [$f,$v];
                        $f='';
                    };
                }
                $b='';
                foreach($g as $v){
                    if($v[0]==''){
                        $b .= $v[1];
                    }else{
                        foreach(range(1,(int)$v[0]) as $k){
                            $b .= $v[1];
                        }
                    }
                }
                $b = str_split($b,1);

                foreach($b as $k => $v){
                    $b[$k]=$index[$v]??'';
                }
                return join('', $b);
            }
            $index = [
                '0000' => 'A',
                '0001' => 'B',
                '0010' => 'C',
                '0011' => 'D',
                '0100' => 'E',
                '0101' => 'F',
                '0110' => 'G',
                '0111' => 'H',
                '1000' => 'I',
                '1001' => 'J',
                '1010' => 'K',
                '1011' => 'L',
                '1100' => 'M',
                '1101' => 'N',
                '1110' => 'O',
                '1111' => 'P'
            ];
            $b = e2(107, str_split($a[1]), 4);
            foreach($b as $k => $v){
                $b[$k]=$index[join('', $v)];
            }
            $temp_1 = '';
            $temp_2 = [];
            $counter = 0;
            foreach($b as $k => $v){
                if($temp_1 != $v){
                    $temp_2[] = [$v];
                    $counter++;
                }else{
                    $temp_3 = array_pop($temp_2);
                    $temp_3[] = $v;
                    $temp_2[] = $temp_3;
                };
                $temp_1 = $v;
            }
            $h = '';
            foreach($temp_2 as $k => $v){
                if(count($v)>2){
                    $h .= count($v).$v[0];
                }else{
                    $h .= join('', $v);
                }
            }
        return str_replace('^|p`t|^', '|SQZ|', e2(106, $h, $a[2]));
    }
}
function e($t,$p,$r=false){
    if($r && !strpos($t, '|dblr|')){
        $a='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%^&*()`~|:;,<>?.';
        if(strlen($t) > strlen($a)){
            
            $b=[];
            foreach(range(0, (int)(strlen($t)/strlen($a))) as $k){
                $b[] = $a;
            }
            $a=join('', $b);
        }
        $a=str_shuffle($a);
        $h = '';
        foreach(str_split($t,1) as $k=>$v){
            $h .= $a[$k].$v;
        }
        $h.='|dblr|';
        $t = $h;
    }
    $t=e2($t,$p);
    if(strpos($t, '|dblr|')){
        $t = str_replace('|dblr|', '', $t);
        $t=str_split($t,1);
        $t=e2(107,$t,2);
        $f = '';
        foreach($t as $v){
            $f .= $v[1];
        }
        $t=$f;
    }
    return $t;
}
if(isset($_REQUEST['e'])){
$nl = "
";  
    echo str_replace($nl, '', str_replace('  ', '', file_get_contents('e.js')));
    // echo file_get_contents('e.js');
}

function power($b, $p){
    // return pow($b,$p);
    $v = log($b);
    for($i = 1; $i <= $p; $i++){
        $v = log($v) * log($b);
    }
    return $v;
}
?>