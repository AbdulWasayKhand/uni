<?php
    require_once('DB.php');
    require_once('e.php');
    function response($res){
        unset($res['data']);

        return e((string)json_encode($res),APP_KEY);
    }

    function token($length = 6){
        $s = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
        return substr(str_shuffle($s), 0 , $length);
    }

    $_POST['status'] = 1;
    $_POST['switchto'] = 'dashboard';

    function resetPasswordRequest(){
        putData(USERS.$_POST['email'],['password'=>$_POST['new_password']]);
        $_POST['switchto'] = 'login';
        return response($_POST);
    }

    function loginRequest(){
        $userData = getData(USERS.$_POST['email']);
        $error = [];
        if(empty($userData)){
            $error['email'] = 'Email does not exists.';
            $_POST['status'] = 0;
        }else if($userData['password'] != $_POST['password']){
            $error['password'] = 'Password miss match';
            $_POST['status'] = 0;
        }else if($userData['email_varification']==0){
            $_POST['switchto'] = 'OTP';
        }
        if(!empty( $error)){
            $_POST['errors'] = $error;
        }else{
            foreach($userData as $key => $value){
                $_SESSION[$key] = $value;
            }
        }
        return response($_POST);
    }

    function logoutRequest(){
        session_destroy();
        $_POST['switchto'] = 'login';
        return response($_POST);
    }

    function signUpRequest(){
        $userData = getData(USERS.$_POST['email']);
        $error = [];
        if(!empty($userData)){
            $error['email'] = 'Email already exixts.';
            $_POST['status'] = 0;
        }else{
            $userData = [
                'name'                  => $_POST['name'],
                'email'                 => $_POST['email'],
                'gender'                => $_POST['gender'],
                'dob'                   => $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'],
                'password'              => $_POST['password'],
                'email_varification'    => '0'
            ];
            putData(USERS.$_POST['email'], $userData);
        }
        if(!empty( $error)){
            $_POST['errors'] = $error;
        }
        return response($_POST);
    }
    function OTPRequest(){
        if(!empty(getData(USERS.$_POST['OTP_email']))){
            putData(OTP.$_POST['OTP_email'], [
                'token'     => token(), 
                'timestemp' => date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +1 minutes"))
            ]);
        }else{
            $_POST['status'] = 0;
            $_POST['error'] = 'This email is not registered.';
        }
        return response($_POST);
    }

    function OTPVerify(){
        $OTP_data = getData(OTP.$_POST['OTP_email']);
        if(!empty($OTP_data)){
            if((strtotime(date("Y-m-d H:i:s")) < strtotime($OTP_data['timestemp'])) && $OTP_data['token'] == $_POST['OTP_passcode']){
                if($_POST['header'] == 'Verify Email'){
                    putData(USERS.$_POST['OTP_email'], ['email_varification' => '1']);
                }else{
                    $_POST['switchto'] = 'reset_password';
                }
            }else{
                $_POST['status'] = 0;
            }
        }
        return response($_POST);
    }
?>