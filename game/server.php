<?php
define('HOST_NAME',"127.0.0.1"); 
define('PORT',"8090");
$null = NULL;

require_once("chathandler.php");
$chatHandler = new ChatHandler();
$socketResource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option($socketResource, SOL_SOCKET, SO_REUSEADDR, 1);
socket_bind($socketResource, HOST_NAME, PORT);
socket_listen($socketResource);
$clientSocketArray = array($socketResource);

while (true) {
	$newSocketArray = $clientSocketArray;
	socket_select($newSocketArray, $null, $null, 0, 10);
	if (in_array($socketResource, $newSocketArray)) {
		$newSocket = socket_accept($socketResource);
		$header = socket_read($newSocket, 1024);
		$userName = explode(" ", $header);
		$userName = explode('?',$userName[1])[1];
		$clientSocketArray[$userName] = $newSocket;
		$chatHandler->doHandshake($header, $newSocket, HOST_NAME, PORT);
		$chatHandler->send(json_encode([
			'to' 		=> $userName,
			'from' 		=> 'server',
			'type' 		=> 'chat',
			'message' 	=> 'connection ACK.',
		]), $userName);
		$newSocketIndex = array_search($socketResource, $newSocketArray);
		unset($newSocketArray[$newSocketIndex]);
	}
	foreach ($newSocketArray as $newSocketArrayResource) {	
		while($newSocketArrayResource && socket_recv($newSocketArrayResource, $socketData, 1024, 0) >= 1){
			if($socketData){
				$socketMessage = $chatHandler->unseal($socketData);
				$messageObj = json_decode($socketMessage);
				if($messageObj){
					$chatHandler->send(json_encode([
						'to' 		=> $messageObj->to ?? '' != '' ? $messageObj->to : $userName,
						'from' 		=> $messageObj->from ?? '' != '' ? $messageObj->from : $userName,
						'type' 		=> $messageObj->type ?? '' != '' ? $messageObj->type : 'chat',
						'message' 	=> $messageObj->message ?? '' != '' ? $messageObj->message : 'echo',
					]), $messageObj->to ?? '' != '' ? $messageObj->to : $userName);
				}
			}
			break 2;
		}
		$socketData = @socket_read($newSocketArrayResource, 1024, PHP_NORMAL_READ);
		if ($socketData === false) { 
			$chatHandler->send(json_encode([]), $userName);
			$newSocketIndex = array_search($newSocketArrayResource, $clientSocketArray);
			unset($clientSocketArray[$newSocketIndex]);			
		}
	}
}
socket_close($socketResource);