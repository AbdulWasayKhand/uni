<?php
    define("BASE_PATH", $baseDIR = str_replace('\\','/',__DIR__));
    define("OTP", BASE_PATH."/OTP/");
    define("USERS", BASE_PATH."/users/");

    function getData($filename){
        if(file_exists($filename.'.cache')){
            return json_decode(file_get_contents($filename.'.cache'),1);
        }
        return [];
    }

    function putData($filename, $data){
        if(file_exists($filename.'.cache')){
            $dumpedData = json_decode(file_get_contents($filename.'.cache'),1);
            foreach($data as $key => $value){
                $dumpedData[$key] = $value;
            }
            file_put_contents($filename.'.cache', json_encode($dumpedData));
        }else{
            file_put_contents($filename.'.cache', json_encode($data));
        }
        return json_decode(file_get_contents($filename.'.cache'),1);
    }

    function deleteData($filename){
        if(file_exists($filename.'.cache')){
            unlink($filename.'.cache');
            return true;
        }
        return false;
    }
?>